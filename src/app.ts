import sha256 from "sha256";
import fs from "fs";

const getFileHash = (path: string) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, buf) => {
      if (err) reject(err);
      else resolve(sha256(buf));
    });
  });
};

const writeFile = (path: string, text: string) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, text, (err) => {
      if (err) reject(err);
      else resolve(null);
    });
  });
};

const main = async () => {
  await writeFile("test.txt", "This is the original file");

  const fileHash = await getFileHash("test.txt");

  console.log("Original file hash is " + fileHash);

  if (Math.round(Math.random())) {
    await writeFile("test.txt", "This is not the original file");
    console.log("File was changed");
  } else {
    console.log("File was not changed");
  }

  const newFileHash = await getFileHash("test.txt");

  console.log("New file hash", newFileHash);

  if (newFileHash === fileHash) console.log("Hash is same");
  else console.log("Hash is not the same");
};

main();
